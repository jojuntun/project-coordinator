import { Component, ElementRef, OnInit, ViewChild, Input } from '@angular/core';
import jsPDF from "jspdf"

@Component({
  selector: 'app-report-item',
  templateUrl: './report-item.component.html',
  styleUrls: ['./report-item.component.css']
})
export class ReportItemComponent implements OnInit {

  @ViewChild('pdfcontent', { read: ElementRef }) pdfContent: ElementRef;

  public SavePDF(): void {
    let content = this.pdfContent.nativeElement;
    let doc = new jsPDF('p', 'mm', 'a4');
    let _elementHandlers = {
      '#editor':function(element,renderer){
        console.log("")
        return true;
      }
    };
    var textTab = doc.splitTextToSize(this.loremIpsumText, 180, {textIndent: 30});
    doc.setFontSize(10)
    doc.text(10, 20, textTab);

    doc.save('test.pdf');
  }

  constructor() { }

  ngOnInit(): void {
  }

  loremIpsumText: string = `Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam tellus felis, auctor et ornare nec, sollicitudin eget urna. Curabitur at egestas diam. Duis posuere commodo lacus sed bibendum. Maecenas euismod neque libero. Mauris a tincidunt ligula. Morbi vitae efficitur lorem. Integer facilisis ultrices sem id ultricies. Mauris cursus augue at nunc rutrum fringilla at ac est. Donec aliquet et libero vitae dapibus. Donec ligula tortor, auctor ac purus eu, pharetra blandit nibh. Nam pharetra volutpat purus et sagittis.

  Pellentesque sit amet tincidunt ipsum. Aliquam pulvinar purus lectus, quis venenatis nunc dictum sit amet. Maecenas aliquet ac magna quis condimentum. Sed facilisis sollicitudin ligula, ac volutpat magna facilisis eget. Sed lobortis nunc sit amet enim accumsan, a pretium ex fringilla. Phasellus vestibulum massa lobortis auctor euismod. Interdum et malesuada fames ac ante ipsum primis in faucibus. In commodo, ipsum in rhoncus porta, urna risus ultricies purus, vel hendrerit felis ante in mauris. Suspendisse leo leo, euismod tempor enim a, feugiat iaculis sem.
  
  Sed tincidunt ut leo sed mollis. Sed aliquam varius luctus. Aliquam convallis fringilla mollis. Suspendisse nec elementum nisl. Suspendisse ut leo feugiat, maximus sapien in, gravida sapien. Praesent tincidunt urna vitae imperdiet elementum. Praesent semper blandit metus pharetra vulputate. Morbi varius ultricies venenatis. Integer gravida a ligula at elementum. Integer commodo in tellus sit amet fermentum.
  
  Curabitur vulputate tempus dolor in lacinia. Nam lobortis nisi eget erat cursus rutrum. In sed volutpat lectus. Ut id quam molestie, rutrum lorem ut, faucibus elit. In posuere magna nec lectus mollis, vel consequat purus volutpat. Etiam pulvinar blandit nulla, non pretium leo fermentum nec. Maecenas tristique blandit fringilla. In mattis, magna feugiat cursus cursus, ex ante semper urna, eget aliquam elit est quis eros.`
}
