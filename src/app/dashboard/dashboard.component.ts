import { Component, OnInit } from '@angular/core';
import { DashboardItem } from '../dashboard-item/dashboard-item.component';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  dashboardItems: DashboardItem[] = [
    {
      name: "Tasks",
      path: "tasks"
    },
    {
      name: "Reports",
      path: "reports"
    },
    {
      name: "Settings",
      path: "settings"
    }
  ]

  constructor() { }

  ngOnInit(): void {
  }

}
