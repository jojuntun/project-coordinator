import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ReportItemComponent } from './report-item/report-item.component';
import { ReportsComponent } from './reports/reports.component';
import { TasksComponent } from './tasks/tasks.component';


const routes: Routes = [
  { path: 'tasks', component: TasksComponent },
  { path: 'reports', component: ReportsComponent },
  { path: 'reports/:id', component: ReportItemComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
