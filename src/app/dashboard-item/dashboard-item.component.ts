import { Component, Input, OnInit } from '@angular/core';

export interface DashboardItem {
  name: string,
  path: string
}

@Component({
  selector: 'app-dashboard-item',
  templateUrl: './dashboard-item.component.html',
  styleUrls: ['./dashboard-item.component.css']
})
export class DashboardItemComponent implements OnInit {
  @Input() dashboardItem: DashboardItem;

  constructor() { }

  ngOnInit(): void {
  }

}
